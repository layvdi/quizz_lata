# Node next
---

## Cấu trúc project
  
    +-- docker                              # thư mục docker hỗ trợ mongo
    +-- log                                 # Nơi log những info request cũng như các exception/eror
    +-- node_modules                        # Nơi kết hợp 2 node_modules của client và server
    +-- packages
    |   +-- client                          # Client repo
    |   +-- server                          # Server repo
    +-- .example.env                        # config cho server
    +-- package.json

## Development setup
1. Pull submodule
    ```git submodule update --init```

2. Đảm bảo đã settup xong client và server trước khi thực hiện bước 3

* [Client](./packages/client/Readme.md)
* [Server](./packages/server/Readme.md)

3. Init config server
    
    ```cp .example.env .env```

4. Install dependencies library

    ```npm run install```

5. Run project as development environment
   
    ```npm run dev```   

==> Url: http://localhost:3000

## How It Works
> - Dự án sẽ kết hợp 2 repo code base lại với nhau, 1 client và 1 server
> - Server sử dụng Node-express và client sủ dụng react-nextjs để làm view template (Tương tự blade laravel).
> - Server sẽ cũng cấp API cho client cũng như hỗ trợ SSR cho client 