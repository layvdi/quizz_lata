# NextJS Code Base

## Công nghệ sử dụng
  1. Next framework v8.03
  2. React JS v16.8
  3. Redux v4.0.1
  4. Redux-saga v1.0.2

## Cấu trúc project
  
    +-- configs                         # Config cho project
    +-- src                             # Source code
    |    +-- .next                       # Build dự án
    |    +-- components                 # Chứa các component
    |    +-- containers                 # Chứa các container
    |    +-- pages                      # Mỗi file sẽ được build thành một page của web
    |    +-- services                   # Chứa các service như call api...
    |    +-- stores                     # Bao gồm những gì liên quan đến store   
    |        +-- actions                # Định nghĩa các action cho dispatch action
    |        +-- constants              # Đinh nghĩa các constant cho action và catch action
    |        +-- reducers               # Định nghĩa các state và xử  lí update state
    |        +-- sagas                  # Nơi bắt các action/dispatch action và xử lí action
    |    +-- utils                      # Cung cấp các hàm tiện tích nhỏ gọn cho dự án
    +-- .eslintrc.js               # config cho eslint check coding convention client

## Development setup

1. Init config app
    
    ```cp configs/appConfig.example.js configs/appConfig.js```

2. Install dependencies library

    ```npm install```

3. Run project as development environment
   
    ```npm run dev```   

==> Url: http://localhost:3000

## Checking code convention
Hiện tại project sử dụng plugin `airbnb` để check code convention cho server
    ```npm run check-code-convention-client```

## Guide

### Workflow

![Drag Racing](docs/workflow.png)

### Coding
1. Tạo page mới
    Vào `src/pages` tạo một component mới, next sẽ auto build compnonent đó thành một page: bạn có thể try cập qua: `http://localhost:3000/new-page` tham khảo thêm `src/pages/index.js` và `src/pages/login.js`
2. Luồng fetch data cho component (SPA) (`src/pages/admin`)
    1. Component connect tới state của store redux
    2. Sau khi Component mounted (componentDidMounted) dispatch một action (khai báo ở `src/stores/actions`) yêu cầu fetch API
    3. Middleware saga ( được khai báo ở `src/stores/sagas`) sẽ bắt action này thực hiện gọi service API (`src/services/api`) để fetch data
    4. Nếu fetch thành công thì saga sẽ dispatch một action để thay đổi state trong store
    4 Component sẽ rerender lại tương ứng với state đã thay đổi
3. Luồng fetch data cho component (SSR) (`src/pages/`)
    Vì là server side rendering nên server sẽ trả thẳng dữ liệu cho component dưới dạng prop nên ko cần phải sử dụng redux để quản lí data 

[Link tham khảo thêm](https://nextjs.org/learn/basics/getting-started)

### Note
1. Tách component nhỏ nhất hoặc nhưng đảm bảo đầy đủ => dễ dàng sử dụng lại component, dễ handle lỗi

2. Hiểu rõ và tách biệt component và container
    * Component: Mọi thứ trông như thế nào (view)
    * Container: Mọi thứ làm việc như thế nào (logic)

3. Luồng dữ liệu luôn là dòng dữ liệu một chiều được quản lí bởi rootState

    * Chỉ container được connect lấy state từ rootstate
    * Muốn thay đổi state của rootstate thì phải dispatch action từ container hoặc saga middleware

## Client theme
  Hiện tại client đang sử dụng React Material, Bạn có thể vào https://material-ui.com để tham khảo thêm các component
