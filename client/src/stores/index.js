import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import rootReducer from './reducers';
import rootSaga from './sagas';
import appConfig from '../../configs/appConfig';

const persistConfig = {
  key: appConfig.appPersistSecretKey,
  storage,
  whitelist: ['authReducer'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  persistedReducer,
  composeWithDevTools(applyMiddleware(sagaMiddleware)),
);

const persistor = persistStore(store);

store.runSagaTask = () => {
  store.sagaTask = sagaMiddleware.run(rootSaga);
};

store.runSagaTask();

export default {
  persistor,
  store,
};
