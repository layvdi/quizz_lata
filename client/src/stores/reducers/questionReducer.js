import * as actionTypes from "../constants";

const initialState = {
  listQuestion: {
    items: []
  }
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.USER_GET_LIST_SUCCESS:
      return {
        ...state,
        listUser: action.data
      };
    default:
      return state;
  }
};

export default userReducer;
