import * as actionTypes from "../constants";

export const requestCreateNewQuestion = data => ({
  data,
  type: actionTypes.QUESTION_CREATE_NEW
});

export const createNewQuestionSuccess = data => ({
  data,
  type: actionTypes.QUESTION_CREATE_NEW_SUCCESS
});

export const createNewQuestionFail = data => ({
  data,
  type: actionTypes.QUESTION_CREATE_NEW_FAIL
});
