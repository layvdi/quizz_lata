import * as actionTypes from '../constants';

export function doLogin(data) {
  return { data, type: actionTypes.AUTH_DO_LOGIN };
}

export function doLoginSuccess(data) {
  return { data, type: actionTypes.AUTH_DO_LOGIN_SUCCESS };
}

export function doLogout() {
  return { type: actionTypes.AUTH_DO_LOG_OUT };
}
