import { takeEvery, call, put, race, delay, select } from "redux-saga/effects";
import * as HttpStatus from "http-status-codes";
import _ from "lodash";

import * as actions from "../actions";
import * as actionTypes from "../constants";
import * as Api from "../../services/api";

function* doCreateQuestion(action) {
  const { data } = action;

  yield put(
    actions.setAppLoading({
      isLoading: true,
      message: "Login..."
    })
  );

  try {
    const token = yield select(state => state.authReducer.token);

    const { posts, timeout } = yield race({
      posts: call(Api.createNewQuestion, data, token),
      timeout: delay(10000)
    });

    yield put(actions.setAppCancelLoading());

    if (timeout) {
      const error = new Error();
      error.status = HttpStatus.REQUEST_TIMEOUT;
      throw error;
    }

    if (posts.status !== HttpStatus.OK) {
      return yield put(
        actions.openSnackbar({
          variant: "error",
          message: "Create question failed",
          autoHideDuration: 3000
        })
      );
    }

    yield put(
      actions.openSnackbar({
        variant: "success",
        message: "Create successful questions",
        autoHideDuration: 3000
      })
    );
  } catch (error) {
    yield put(actions.throwError(error));
  }
}

export default function* watchAuth() {
  yield takeEvery(actionTypes.QUESTION_CREATE_NEW, doCreateQuestion);
}
