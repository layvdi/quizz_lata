import { all } from "redux-saga/effects";

import watchAuth from "./authSaga";
import watchUser from "./userSaga";
import watchApp from "./appSaga";
import watchQuestion from "./questionSaga";

export default function* rootSaga() {
  yield all([watchAuth(), watchUser(), watchApp(), watchQuestion()]);
}
