import React, { Component } from "react";
import { connect } from "react-redux";
import propTypes from "prop-types";
import Router from "next/router";

import AdminLayoutComponent from "../../components/admin/layout/AdminLayoutComponent";
import * as actions from "../../stores/actions";

class AdminContainer extends Component {
  handleLogout = () => {
    this.props.doLogout();
  };

  render() {
    if (!process.browser) {
      return null;
    }

    if (!this.props.authReducer.token) {
      Router.push("/login");
      return null;
    }

    return (
      <AdminLayoutComponent handleLogout={this.handleLogout}>
        {this.props.children}
      </AdminLayoutComponent>
    );
  }
}

const mapStateToProps = state => ({
  authReducer: state.authReducer
});

const mapDispatchToProps = dispatch => ({
  doLogout: () => dispatch(actions.doLogout())
});

AdminContainer.propTypes = {
  children: propTypes.any.isRequired,
  authReducer: propTypes.object.isRequired,
  doLogout: propTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminContainer);
