import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import SnackbarComponent from '../components/commons/snackbar';
import { closeSnackbar } from '../stores/actions/appAction';

class SnackbarContainer extends Component {
    handleCloseSnackbar = (event, reason) => {
      if (reason === 'clickaway') {
        return;
      }

      this.props.closeSnackbar();
    }

    render() {
      return <SnackbarComponent handleCloseSnackbar={this.handleCloseSnackbar} snackbar={this.props.snackbar} />;
    }
}

const mapDispatchToProps = dispatch => ({
  closeSnackbar: () => dispatch(closeSnackbar()),
});

const mapStateToProps = state => ({
  snackbar: state.appReducer.appSnackbar,
});

SnackbarContainer.propTypes = {
  closeSnackbar: PropTypes.func.isRequired,
  snackbar: PropTypes.shape({
    isOpen: PropTypes.bool,
    anchorOrigin: PropTypes.object,
    autoHideDuration: PropTypes.number,
    variant: PropTypes.string,
    message: PropTypes.any,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(SnackbarContainer);
