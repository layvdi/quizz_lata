import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SnackbarContainer from './snackbarContainer';

class RootContainer extends Component {
  render() {
    return (
      <div>
        <SnackbarContainer />
        {
          this.props.children
        }
      </div>
    );
  }
}

RootContainer.propTypes = {
  children: PropTypes.object,
};

RootContainer.defaultProps = {
  children: null,
};

export default RootContainer;
