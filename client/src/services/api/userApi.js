import makeRequest from './apiServices';

export async function getListUser(data, accessToken) {
  let url = '/api/v1/users';

  if (data.currentPage) {
    url = `${url}&page=${data.currentPage}`;
  }

  if (data.perPage) {
    url = `${url}&limit=${data.perPage}`;
  }

  const options = {};
  options.method = 'GET';
  options.url = url;
  options.headers = {
    Authorization: `Bearer ${accessToken}`
  };

  return makeRequest(options);
}

export async function getUser(id, headers) {
  const url = `/api/v1/users/${id}`;

  const options = {};
  options.method = 'GET';
  options.url = url;
  options.headers = headers;

  return makeRequest(options);
}
