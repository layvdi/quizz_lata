import makeRequest from './apiServices';

export async function login(postData, headers) {
  const options = {};
  options.method = 'POST';
  options.url = '/api/v1/users/login';
  options.headers = headers;
  options.data = postData;

  return makeRequest(options);
}
