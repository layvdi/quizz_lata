import axios from "axios";
import HttpStatus from "http-status-codes";

import appConfig from "../../../configs/appConfig";

const makeRequest = async options => {
  const newOptions = options;
  newOptions.url = `${appConfig.apiEndPoint}${options.url}`;
  newOptions.timeout = appConfig.requestTimeout;

  try {
    const response = await axios(newOptions);
    return response;
  } catch (error) {
    if (error.response && error.response.status) {
      return error.response;
    }

    if (error.code == "ECONNABORTED") {
      const errorThrow = new Error();
      errorThrow.status = HttpStatus.REQUEST_TIMEOUT;

      throw errorThrow;
    }

    throw error.response;
  }
};

export default makeRequest;
