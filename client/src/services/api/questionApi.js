import makeRequest from "./apiServices";

export async function createNewQuestion(data, accessToken) {
  const options = {};
  options.method = "POST";
  options.url = "/api/v1/questions";
  options.headers = {
    Authorization: `Bearer ${accessToken}`
  };
  options.data = data;

  return makeRequest(options);
}
