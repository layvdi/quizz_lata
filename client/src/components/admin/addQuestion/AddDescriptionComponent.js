import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import { Editor } from "react-draft-wysiwyg";
import { EditorState } from "draft-js";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import PropTypes from "prop-types";

import AutoCompleteComponent from "../../commons/autoComplete/AutoCompleteComponent";

const useStyles = makeStyles(theme => ({
  wrapper: {
    // border: "1px #F1F1F1 solid",
    height: "100%"
  },
  wrapperQuestionType: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  formControlSelect: {
    minWidth: "200px",
    margin: theme.spacing(1)
  },
  wrapperTag: {
    border: "1px #F1F1F1 solid",
    color: "#5d5959",
    minHeight: "10vh"
  }
}));

const AddDescriptionComponent = props => {
  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);

  const classes = useStyles();

  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);

  return (
    <div className={classes.wrapper}>
      <br />
      <br />
      <div className={classes.wrapperQuestionType}>
        <FormControl variant="outlined" className={classes.formControlSelect}>
          <InputLabel ref={inputLabel} htmlFor="outlined-question-type">
            Select Question Type
          </InputLabel>
          <Select
            value={props.questionData.type}
            color="secondary"
            onChange={event =>
              props.handleChangeQuestionData({
                type: event.target.value
              })
            }
            input={
              <OutlinedInput
                color="secondary"
                name="age"
                labelWidth={labelWidth}
                id="outlined-question-type"
              />
            }
          >
            <MenuItem value="oneChoice">One choice Question</MenuItem>
            <MenuItem value="multiChoice">Multi choice Question</MenuItem>
          </Select>
        </FormControl>
      </div>
      <br />
      <br />
      <br />
      <AutoCompleteComponent
        multi={props.questionData.tag}
        setMulti={value => props.handleChangeQuestionData({ tag: value })}
      />
    </div>
  );
};

AddDescriptionComponent.propTypes = {
  handleChangeQuestionData: PropTypes.func.isRequired,
  questionData: PropTypes.object.isRequired
};

export default AddDescriptionComponent;
