import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import { Editor } from "react-draft-wysiwyg";
import { EditorState } from "draft-js";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";

const useStyles = makeStyles(theme => ({
  wrapper: {
    border: "1px #F1F1F1 solid",
    height: "50vh"
  }
}));

const AddQuestionContentComponent = props => {
  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  function onEditorStateChange(editorStateObj) {
    setEditorState(editorStateObj);
  }

  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <Editor
        initialContentState={props.questionData.content}
        editorState={editorState}
        toolbarClassName="toolbarClassName"
        editorClassName="editorClassName"
        onEditorStateChange={onEditorStateChange}
        onContentStateChange={contentStateObj =>
          props.handleChangeQuestionData({ content: contentStateObj })
        }
      />
    </div>
  );
};

AddQuestionContentComponent.propTypes = {
  questionData: PropTypes.object.isRequired,
  handleChangeQuestionData: PropTypes.func.isRequired
};

export default AddQuestionContentComponent;
