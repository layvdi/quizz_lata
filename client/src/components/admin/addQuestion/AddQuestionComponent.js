import React, { useState } from "react";
import PropTypes from "prop-types";
import SwipeableViews from "react-swipeable-views";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";

import { json } from "body-parser";
import AddQuestionContentComponent from "./AddQuestionContentComponent";
import AddAnswerComponent from "./AddAnswerComponent";
import AddDescriptionComponent from "./AddDescriptionComponent";

function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired
};

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: "20px",
    height: "70vh",
    // display: "flex"
    position: "relative"
  },
  tab: {},
  footer: {
    display: "flex",
    alignSelf: "flex-end",
    justifyContent: "space-between",
    padding: theme.spacing(2),
    position: "absolute",
    bottom: 0,
    width: "100%"
  }
}));

const AddQuestionComponent = props => {
  const classes = useStyles();
  const theme = useTheme();
  const [indexTab, setIndexTab] = useState(2);
  const [questionData, setQuestionData] = useState({
    content: "",
    title: "",
    type: "",
    tag: [],
    answer: [
      {
        content: "Option 1",
        canDelete: false,
        isRight: false
      },
      {
        content: "Option 2",
        canDelete: false,
        isRight: false
      }
    ]
  });

  function handleChangeQuestionData(newDataProperties) {
    setQuestionData({
      ...questionData,
      ...newDataProperties
    });
  }

  return (
    <Container maxWidth="lg">
      <Typography>Add Question</Typography>
      <Paper className={classes.root}>
        <Tabs
          className={classes.tab}
          value={indexTab}
          onChange={(event, newValue) => setIndexTab(newValue)}
          indicatorColor="secondary"
          textColor="secondary"
          variant="fullWidth"
        >
          <Tab label="description" />
          <Tab label="Add Content" />
          <Tab label="Add Answers" />
        </Tabs>
        <SwipeableViews
          axis={theme.direction === "rtl" ? "x-reverse" : "x"}
          index={indexTab}
          onChangeIndex={index => setIndexTab(index)}
        >
          <TabContainer dir={theme.direction}>
            <AddDescriptionComponent
              questionData={questionData}
              handleChangeQuestionData={handleChangeQuestionData}
            />
          </TabContainer>
          <TabContainer dir={theme.direction}>
            <AddQuestionContentComponent
              questionData={questionData}
              handleChangeQuestionData={handleChangeQuestionData}
            />
          </TabContainer>
          <TabContainer
            dir={theme.direction}
            style={{ backgroundColor: "red" }}
          >
            <AddAnswerComponent
              questionData={questionData}
              handleChangeQuestionData={handleChangeQuestionData}
            />
          </TabContainer>
        </SwipeableViews>
        <div className={classes.footer}>
          <Button
            disabled={indexTab <= 0}
            onClick={() => setIndexTab(indexTab - 1)}
            variant="contained"
          >
            Back
          </Button>
          {indexTab == 2 ? (
            <Button
              onClick={() =>
                props.requestCreateNewQuestion({
                  ...questionData,
                  content: JSON.stringify(questionData.content)
                })
              }
              color="primary"
              variant="contained"
            >
              Finish
            </Button>
          ) : (
            <Button
              disabled={indexTab >= 2}
              onClick={() => setIndexTab(indexTab + 1)}
              color="primary"
              variant="contained"
            >
              Next
            </Button>
          )}
        </div>
      </Paper>
    </Container>
  );
};

AddQuestionComponent.propTypes = {
  requestCreateNewQuestion: PropTypes.func.isRequired
};

export default AddQuestionComponent;
