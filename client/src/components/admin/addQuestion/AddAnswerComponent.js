import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import { Editor } from "react-draft-wysiwyg";
import { EditorState } from "draft-js";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Checkbox from "@material-ui/core/Checkbox";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "@material-ui/core/IconButton";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import { Typography } from "@material-ui/core";
import PropTypes from "prop-types";

const useStyles = makeStyles(theme => ({
  wrapper: {
    border: "1px #F1F1F1 solid",
    height: "50vh",
    flexDirection: "row",
    overflow: "auto"
  },
  answerItem: {
    padding: "5px",
    display: "flex",
    flexDirection: "row"
  },
  IconClose: {
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
    minWidth: "50px"
  },
  IconWrapper: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  answerChoice: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    minWidth: "50px"
  }
}));

const AddAnswerComponent = props => {
  const [editorState, setEditorState] = useState(EditorState.createEmpty());
  const [contentState, setContentState] = useState({});

  function onEditorStateChange(editorStateObj) {
    setEditorState(editorStateObj);
  }

  const classes = useStyles();

  return (
    <ul className={classes.wrapper}>
      {props.questionData.answer.map((answer, index) => {
        return (
          <li key={index} className={classes.answerItem}>
            <div className={classes.answerChoice}>
              <Checkbox
                checked={answer.isRight}
                onChange={(event, checked) => {
                  const newAnswers = props.questionData.answer;
                  newAnswers[index].isRight = checked;
                  props.handleChangeQuestionData({
                    answer: newAnswers
                  });
                }}
              />
              <Typography>{answer.isRight ? "true" : "false"}</Typography>
            </div>
            <TextField
              value={answer.content}
              margin="normal"
              fullWidth
              onChange={e => {
                const newAnswers = props.questionData.answer;
                newAnswers[index].content = e.target.value;
                props.handleChangeQuestionData({
                  answer: newAnswers
                });
              }}
              variant="outlined"
              inputProps={{ "aria-label": "bare" }}
            />
            <div className={classes.IconClose}>
              {answer.canDelete ? (
                <IconButton
                  onClick={() =>
                    props.handleChangeQuestionData({
                      answer: props.questionData.answer.filter(
                        (answer, indexAnswer) => indexAnswer !== index
                      )
                    })
                  }
                >
                  <CloseIcon style={{ color: "red" }} />
                </IconButton>
              ) : null}
            </div>
          </li>
        );
      })}

      <Grid item md={12} className={classes.IconWrapper}>
        <Fab
          color="primary"
          onClick={() =>
            props.handleChangeQuestionData({
              answer: [
                ...props.questionData.answer,
                {
                  content: `Option ${props.questionData.answer.length + 1}`,
                  canDelete: true,
                  isRight: false
                }
              ]
            })
          }
        >
          <AddIcon />
        </Fab>
      </Grid>
    </ul>
  );
};

AddAnswerComponent.propTypes = {
  handleChangeQuestionData: PropTypes.func.isRequired
};

export default AddAnswerComponent;
