import React, { useState } from "react";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import makeStyles from "@material-ui/styles/makeStyles";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import ImportExportIcon from "@material-ui/icons//ImportExport";
import SpeedDial from "@material-ui/lab/SpeedDial";
import SpeedDialAction from "@material-ui/lab/SpeedDialAction";
import SpeedDialIcon from "@material-ui/lab/SpeedDialIcon";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "next/link";

import DataTableComponent from "../../commons/dataTable/dataTableComponent";

const useStyles = makeStyles(theme => ({
  noItemWrapper: {
    textAlign: "center",
    padding: theme.spacing(4)
  },
  btnAction: {
    display: "inline-block",
    position: "absolute",
    right: "0",
    top: "10px"
  },
  btnActionWrapper: {
    position: "relative"
  }
}));

const items = [
  {
    content:
      "Eu veniam cillum Lorem reprehenderit adipisicing. Non fugiat ullamco qui esse Lorem. Sunt culpa laboris incididunt culpa. Irure eu excepteur aliqua nulla nulla pariatur minim sit occaecat ea consectetur. Esse amet ut ipsum labore. Irure velit Lorem deserunt sit amet aliqua. Adipisicing tempor officia ullamco exercitation ut ullamco excepteur sit ipsum velit sit sit sit.",
    type: "Multi choice",
    tags: ["fresher", "php", "front-end"],
    status: "active",
    addedBy: "admin",
    addedAt: "2019-04-02"
  }
];

const column = [
  {
    header: {
      label: "Content",
      width: 40
    },
    accessor: {
      key: "content"
    }
  },
  {
    header: {
      label: "Type"
    },
    accessor: {
      key: "type"
    }
  },
  {
    header: {
      label: "Tags"
    },
    accessor: {
      key: "tags"
    }
  },
  {
    header: {
      label: "Status"
    },
    accessor: {
      key: "status"
    }
  },
  {
    header: {
      label: "Added By"
    },
    accessor: {
      key: "addedBy"
    }
  },
  {
    header: {
      label: "Date Added"
    },
    accessor: {
      key: "addedAt"
    }
  }
];

const dataTable = {
  items,
  column
};

export default function test() {
  const [openAction, toggleAction] = useState(false);

  const classes = useStyles();

  return (
    <Container maxWidth="xl">
      <Paper>
        {items && items.length ? (
          <DataTableComponent dataTable={dataTable} showNo />
        ) : (
          <Typography
            variant="h6"
            component="p"
            className={classes.noItemWrapper}
          >
            There are no items to display
          </Typography>
        )}
      </Paper>
      <div className={classes.btnActionWrapper}>
        <SpeedDial
          className={classes.btnAction}
          ariaLabel="Action"
          open={openAction}
          color="secondary"
          onClick={() => toggleAction(!openAction)}
          icon={<SpeedDialIcon />}
        >
          <SpeedDialAction
            tooltipTitle="Add new Question"
            icon={
              <Link href="/admin/questions/add-questions">
                <AddIcon />
              </Link>
            }
          />
          <SpeedDialAction
            tooltipTitle="Import question"
            icon={<ImportExportIcon />}
          />
        </SpeedDial>
      </div>
    </Container>
  );
}
