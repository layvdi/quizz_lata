import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { makeStyles } from "@material-ui/core/styles";
import Badge from "@material-ui/core/Badge";
import clsx from "clsx";
import Typography from "@material-ui/core/Typography";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/Inbox";
import DashboardIcon from "@material-ui/icons/Dashboard";
import QuestionAnswerIcon from "@material-ui/icons/QuestionAnswer";
import SessionIcon from "@material-ui/icons/Reorder";
import UserIcon from "@material-ui/icons/SupervisorAccount";
import Router from "next/router";
import Link from "next/link";

const useStyles = makeStyles(theme => {
  return {
    toolbar: {
      ...theme.mixins.toolbar,
      backgroundImage: "url(../../../static/images/logo.jpg)",
      backgroundRepeat: "no-repeat",
      backgroundSize: "contain",
      backgroundPosition: "center"
    },
    MenuItemWrapper: {
      display: "flex",
      alignItems: "center",
      width: "100%",
      height: "100%"
    },
    activeMenu: {
      "&::before": {
        content: "''",
        width: "2px",
        backgroundColor: theme.palette.primary.light,
        marginLeft: "-7px",
        marginRight: "7px",
        height: "30px"
      }
    },
    activeBackgroundMenu: {
      backgroundColor: theme.palette.grey[100]
    }
  };
});

const listMenu = [
  {
    title: "Dashboard",
    url: "/admin",
    icon: DashboardIcon
  },
  {
    title: "Questions",
    url: "/admin/questions",
    icon: QuestionAnswerIcon
  },
  {
    title: "Session",
    url: "/admin/sessions",
    icon: SessionIcon
  },
  {
    title: "Users",
    url: "/admin/users",
    icon: UserIcon
  }
];

export default function ResponsiveDrawer(props) {
  const [currentRoute, setCurrentRoute] = useState("");
  const classes = useStyles();

  useEffect(() => {
    setCurrentRoute(Router.router.route);
  });

  return (
    <React.Fragment>
      <div className={classes.toolbar} />
      <Divider />
      <List>
        {listMenu.map((menuItem, index) => {
          const Icon = menuItem.icon;
          let isActive = false;

          if (currentRoute === menuItem.url) {
            isActive = true;
          }

          return (
            <Link href={menuItem.url} key={index}>
              <ListItem
                button
                key={index}
                className={isActive ? classes.activeBackgroundMenu : ""}
              >
                <div
                  className={clsx(
                    classes.MenuItemWrapper,
                    isActive && classes.activeMenu
                  )}
                >
                  <ListItemIcon>
                    <Icon color={isActive ? "primary" : undefined} />
                  </ListItemIcon>
                  <ListItemText primary={menuItem.title} />
                </div>
              </ListItem>
            </Link>
          );
        })}
      </List>
    </React.Fragment>
  );
}
