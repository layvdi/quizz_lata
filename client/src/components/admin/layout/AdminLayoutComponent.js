import React, { useState, useEffect } from "react";
import Container from "@material-ui/core/Container";
import MenuBarComponent from "./MenuBarComponent";
import SlideBarComponent from "./SlideBarComponent";

export default props => {
  const [isOpenDraw, toggleDraw] = useState(true);
  const [isOnTop, setIsOnTop] = useState(false);

  return (
    <div
      style={{
        width: "100%",
        display: "flex",
        minHeight: "100vh"
      }}
    >
      <SlideBarComponent isOpenDraw={isOpenDraw} toggleDraw={toggleDraw} />
      <div style={{ width: "100%" }}>
        <MenuBarComponent
          isOnTop={isOnTop}
          isOpenDraw={isOpenDraw}
          toggleDraw={toggleDraw}
          handleLogout={props.handleLogout}
        />
        <br />
        <main>{props.children}</main>
      </div>
    </div>
  );
};
