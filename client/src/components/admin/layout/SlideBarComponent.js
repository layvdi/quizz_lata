import React from "react";
import PropTypes from "prop-types";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { makeStyles } from "@material-ui/core/styles";
import Badge from "@material-ui/core/Badge";
import clsx from "clsx";
import Typography from "@material-ui/core/Typography";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/Inbox";
import MailIcon from "@material-ui/icons/Mail";

import MenuBarListItemComponent from "./MenuBarListItemComponent";

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0
    }
  },
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up("sm")]: {
      width: `calc(100% - ${drawerWidth}px)`
    }
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      display: "none"
    }
  },
  drawerClose: {
    width: 0
  },
  toolbar: {
    ...theme.mixins.toolbar,
    backgroundImage: "url(../../../static/images/logo.jpg)",
    backgroundRepeat: "no-repeat",
    backgroundSize: "contain",
    backgroundPosition: "center"
  },
  drawerPaper: {
    width: drawerWidth
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3)
  }
}));

export default function ResponsiveDrawer(props) {
  const classes = useStyles();
  return (
    <Drawer
      variant="persistent"
      anchor="left"
      open={props.isOpenDraw}
      className={clsx(classes.drawer, !props.isOpenDraw && classes.drawerClose)}
      classes={{
        paper: classes.drawerPaper
      }}
      onClose={() => props.toggleDraw(!props.isOpenDraw)}
      ModalProps={{
        keepMounted: true
      }}
    >
      <MenuBarListItemComponent />
    </Drawer>
  );
}

ResponsiveDrawer.propTypes = {
  toggleDraw: PropTypes.func.isRequired,
  isOpenDraw: PropTypes.bool.isRequired
};
