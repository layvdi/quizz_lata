import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import CloseIcon from "@material-ui/icons/Close";
import PropTypes from "prop-types";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "next/link";
import Typography from "@material-ui/core/Typography";
import Router from "next/router";
import MenuItem from "@material-ui/core/MenuItem";
import Badge from "@material-ui/core/Badge";
import NotificationsIcon from "@material-ui/icons/Notifications";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Menu from "@material-ui/core/Menu";

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1
  },
  toolbarWrapper: {
    justifyContent: "space-between",
    display: "flex"
  },
  toolbarWrapperBread: {
    display: "flex",
    alignItems: "center"
  },
  capitalizeText: {
    textTransform: "capitalize",
    color: "white"
  },
  toolBarMenuRight: {
    display: "flex",
    flexDirection: "row"
  }
}));

export default function ButtonAppBar(props) {
  const [currentRoute, setCurrentRoute] = useState("");
  const [openMenuUser, toggleOpenMenuUser] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);

  useEffect(() => {
    setCurrentRoute(Router.router.route);
  });

  const classes = useStyles();

  function renderBreadcrumbs() {
    const breadEls = [];

    const listRoutes = currentRoute.split("/");

    listRoutes.map((text, index) => {
      if (!text) {
        return;
      }

      if (listRoutes.length === index + 1) {
        breadEls.push(
          <Typography key={index} className={classes.capitalizeText}>
            {text}
          </Typography>
        );
        return;
      }

      breadEls.push(
        <Link href={`/${text}`} key={index}>
          <p
            style={{
              textTransform: "capitalize",
              color: "white",
              textDecoration: "underline",
              cursor: "pointer"
            }}
          >
            {text}
          </p>
        </Link>
      );
    });

    return breadEls;
  }

  return (
    <div className={classes.root}>
      <AppBar position="relative">
        <Toolbar className={classes.toolbarWrapper}>
          <div className={classes.toolbarWrapperBread}>
            <IconButton
              onClick={() => props.toggleDraw(!props.isOpenDraw)}
              color="inherit"
              aria-label="Menu"
            >
              {props.isOpenDraw ? <CloseIcon /> : <MenuIcon />}
            </IconButton>
            <Breadcrumbs aria-label="Breadcrumb" style={{ color: "white" }}>
              {renderBreadcrumbs()}
            </Breadcrumbs>
          </div>
          <div className={classes.toolBarMenuRight}>
            <IconButton aria-label="Show 11 new notifications" color="inherit">
              <Badge badgeContent={11} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <IconButton
              aria-label="Account of current user"
              aria-controls="primary-search-account-menu"
              aria-haspopup="true"
              color="inherit"
              buttonRef={node => setAnchorEl(node)}
              onClick={() => toggleOpenMenuUser(!openMenuUser)}
            >
              <AccountCircle />
              <Menu
                anchorEl={anchorEl}
                open={openMenuUser}
                onClose={() => toggleOpenMenuUser(!openMenuUser)}
              >
                <MenuItem>Profile</MenuItem>
                <MenuItem onClick={() => props.handleLogout()}>Logout</MenuItem>
              </Menu>
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}

ButtonAppBar.propTypes = {
  toggleDraw: PropTypes.func.isRequired,
  isOpenDraw: PropTypes.bool.isRequired
};
