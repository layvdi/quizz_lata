import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  createStyles,
  withStyles,
  Paper,
  Table,
  TableRow,
  TableBody,
  TableHead,
  TableCell,
  TableFooter,
  TablePagination
} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import _ from "lodash";
import LinesEllipsis from "react-lines-ellipsis";

const styles = theme =>
  createStyles({
    root: {
      width: "100%",
      marginTop: theme.spacing.unit * 3,
      overflowX: "auto"
    },
    table: {
      minWidth: 700
    }
  });

class DataTableComponent extends Component {
  getWidthPerColumn = () => {
    const columnDefinedWidthPercent = this.props.dataTable.column.filter(
      value => value.header.width > 0
    );

    const columnWidthPercentRemain =
      100 -
      columnDefinedWidthPercent.reduce(
        (total, value) => value.header.width + total,
        0
      );

    const perColumnWidthPercent =
      columnWidthPercentRemain /
      (this.props.dataTable.column.length - columnDefinedWidthPercent.length);

    return perColumnWidthPercent;
  };

  renderTableHead = () => {
    const perColumnWidthPercent = this.getWidthPerColumn();

    let rawHead = this.props.dataTable.column.map((value, index) => (
      <TableCell
        width={`${
          value.header.width ? value.header.width : perColumnWidthPercent
        }%`}
        key={index}
      >
        {value.header.label}
      </TableCell>
    ));

    if (this.props.showNo) {
      rawHead = [<TableCell width="10px">No</TableCell>, ...rawHead];
    }

    return rawHead;
  };

  renderTableCellData = (rowObject, rowIndex) => {
    let rawCell = this.props.dataTable.column.map((value, index) => {
      if (_.isFunction(value.cell)) {
        return <TableCell key={index}>{value.cell(rowObject)}</TableCell>;
      }
      if (_.isFunction(value.cell)) {
        return <TableCell key={index}>{value.cell(rowObject)}</TableCell>;
      }
      return (
        <TableCell key={index} numeric>
          <LinesEllipsis
            text={rowObject[value.accessor.key]}
            maxLine="1"
            ellipsis="..."
            trimRight
            basedOn="letters"
          />
          {/* rowObject[value.accessor.key]} */}
        </TableCell>
      );
    });

    if (this.props.showNo) {
      rawCell = [<TableCell>{rowIndex + 1}</TableCell>, ...rawCell];
    }

    return rawCell;
  };

  renderTableRowData = () =>
    this.props.dataTable.items.map((value, index) => (
      <TableRow key={index}>{this.renderTableCellData(value, index)}</TableRow>
    ));

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>{this.renderTableHead()}</TableRow>
          </TableHead>
          <TableBody>{this.renderTableRowData()}</TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                colSpan={
                  this.props.dataTable.column.length +
                  (this.props.showNo ? 1 : 0)
                }
                count={100}
                rowsPerPage={10}
                page={2}
                SelectProps={{
                  inputProps: { "aria-label": "Rows per page" },
                  native: true
                }}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </Paper>
    );
  }
}

DataTableComponent.propTypes = {
  dataTable: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(DataTableComponent);
