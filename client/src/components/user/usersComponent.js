import React, { Component } from 'react';
import propTypes from 'prop-types';
import { Fab } from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete';

import UserDeleteConfirmDialog from './userDeleteConfirmDialog'
import DataTableComponent from '../commons/dataTable/dataTableComponent';

class UsersComponent extends Component {
    state = {
      dataTableColumn: [],
      itemSelected: {},
      openDialogDeleteUser: false,
    }

    componentDidMount() {
      const listDataTableColumn = this.defineDataTableColumn();

      this.setState({
        dataTableColumn: listDataTableColumn,
      });
    }

    defineDataTableColumn = () => [
      {
        header: {
          label: 'ID',
        },
        cell: props => (
            <a
              href="#"
              onClick={
                () => {
                  window.location.href=`/admin/user?id=${props._id}`
                }
              }
            >
              {props._id}
            </a>
        ),
        accessor: {
          key: '_id',
        },
      },
      {
        header: {
          label: 'First Name',
        },
        accessor: {
          key: 'firstName',
        },
      },
      {
        header: {
          label: 'Last Name',
        },
        accessor: {
          key: 'lastName',
        },
      },
      {
        header: {
          label: 'Email',
        },
        accessor: {
          key: 'email',
        },
      },
      {
        header: {
          label: 'Date Added',
        },
        accessor: {
          key: 'createdAt',
        },
      },
      {
        header: {
          label: 'Action',
        },
        cell: (props) => (
          <Fab size="small" aria-label="Add" color="secondary"
            onClick={
              () => {
                this.handleToggleDialogDeleteUser();
                this.setState({itemSelected: props})
              }
            }
          >
            <DeleteIcon fontSize="small"/>
          </Fab>
        ),
      },
    ]

    handleToggleDialogDeleteUser = () => {
      this.setState({
        openDialogDeleteUser: !this.state.openDialogDeleteUser
      })
    }

    render() {
      const dataTable = {
        items: this.props.listUser.items,
        column: this.state.dataTableColumn,
      };

      return (
        <div>
          <UserDeleteConfirmDialog
            user={this.state.itemSelected}
            openDialogDeleteUser={this.state.openDialogDeleteUser}
            handleToggleDialogDeleteUser={this.handleToggleDialogDeleteUser}
          />
          <DataTableComponent dataTable={dataTable} />
        </div>
      )
    }
}

UsersComponent.propTypes = {
  listUser: propTypes.object.isRequired,
};

export default UsersComponent;
