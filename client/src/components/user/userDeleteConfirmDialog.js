import React, { Component } from 'react';
import { Dialog, Slide, Button, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';
import PropTypes from 'prop-types';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class UserDeleteConfirmDialog extends Component {
  render() {
    return (
      <Dialog
        open={this.props.openDialogDeleteUser}
        TransitionComponent={Transition}
        keepMounted
        onClose={this.props.handleToggleDialogDeleteUser}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          Detele User
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            <span>
            Are you want to delete
            <strong>
              {` ${this.props.user.firstName} ${this.props.user.lastName}`}
            </strong>
            </span>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleToggleDialogDeleteUser} color="primary">
            Cancel
          </Button>
          <Button onClick={this.handleToggleDialogDeleteUser} color="primary">
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

UserDeleteConfirmDialog.propTypes = {
  openDialogDeleteUser: PropTypes.bool.isRequired,
  user: PropTypes.object.isRequired,
  handleToggleDialogDeleteUser: PropTypes.func.isRequired,
}

export default UserDeleteConfirmDialog;