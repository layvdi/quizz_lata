import React, { useState, useEffect } from "react";

import MenuBarComponent from "../layout/MenuBarComponent";
import SlideBarComponent from "../layout/SlideBarComponent";
import ListViewQuizComponent from "../listViewQuiz/ListViewQuizComponent";
import CountdownAlwayOnTop from "../countdown/CountdownAlwayOnTop";

export default () => {
  const [isOpenDraw, toggleDraw] = useState(true);
  const [isOnTop, setIsOnTop] = useState(false);

  useEffect(() => {
    window.onscroll = () => {
      if (window.pageYOffset === 0) {
        setIsOnTop(true);
      } else if (isOnTop == true) {
        setIsOnTop(false);
      }
    };
  });

  return (
    <div style={{ width: "100%", display: "flex" }}>
      {!isOnTop ? <CountdownAlwayOnTop /> : null}

      <SlideBarComponent isOpenDraw={isOpenDraw} toggleDraw={toggleDraw} />
      <div style={{ width: "100%" }}>
        <MenuBarComponent
          isOnTop={isOnTop}
          isOpenDraw={isOpenDraw}
          toggleDraw={toggleDraw}
        />
        <br />
        <ListViewQuizComponent />
      </div>
    </div>
  );
};
