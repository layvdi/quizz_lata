import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import PropTypes from "prop-types";

const useStyles = makeStyles(theme => ({
  card: {},
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    backgroundColor: red[500]
  },
  answerWrapper: {
    display: "flex",
    flexDirection: "column"
  },
  answerItem: {
    width: "fit-content"
  },
  cardFooter: {
    marginLeft: "auto"
  }
}));

export default function RecipeReviewCard(props) {
  const classes = useStyles();

  return (
    <Card className={classes.card} style={props.style} raised>
      <CardHeader title=" Heat 1/2 cup of the broth in a pot until simmering, add saffron and set aside for 10 minutes." />
      <CardContent className={classes.answerWrapper}>
        <FormControlLabel
          control={<Checkbox value="checkedA" />}
          className={classes.answerItem}
          label="Answer 1"
        />
        <FormControlLabel
          control={<Checkbox value="checkedA" />}
          label="Answer 2"
        />
        <FormControlLabel
          control={<Checkbox value="checkedA" />}
          label="Answer 3"
        />
        <FormControlLabel
          control={<Checkbox value="checkedA" />}
          label="Answer 4"
        />
      </CardContent>
      <CardActions>
        <Typography variant="p" component="p" className={classes.cardFooter}>
          3/16
        </Typography>
      </CardActions>
    </Card>
  );
}

RecipeReviewCard.propTypes = {
  style: PropTypes.object
};

RecipeReviewCard.defaultProps = {
  style: PropTypes.object
};
