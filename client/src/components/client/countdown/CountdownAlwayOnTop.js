import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import CountDownComponent from "./CountDownComponent";

const useStyles = makeStyles(() => ({
  timerOnTop: {
    position: "fixed",
    top: "0",
    width: "100%",
    height: "100px",
    display: "flex",
    justifyContent: "flex-end"
  },
  timerWrapper: {
    display: "inline-block",
    marginRight: "30px",
    background: "#0f2027" /* fallback for old browsers */,
    background:
      "-webkit-linear-gradient(to top, #0f2027, #203a43, #2c5364)" /* Chrome 10-25, Safari 5.1-6 */,
    background:
      "linear-gradient(to top, #0f2027, #203a43, #2c5364)" /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */,
    height: "30px",
    width: "100px",
    borderBottomLeftRadius: "10px",
    borderBottomRightRadius: "10px"
  }
}));

export default () => {
  const classes = useStyles();
  return (
    <div className={classes.timerOnTop}>
      <div className={classes.timerWrapper}>
        <CountDownComponent style={{ color: "white" }} />
      </div>
    </div>
  );
};
