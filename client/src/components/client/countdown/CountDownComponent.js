import React, { useState, useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { useTransition, animated } from "react-spring";

const useStyles = makeStyles(() => ({
  timer: {
    justifyContent: "center",
    display: "flex",
    alignItems: "center"
  }
}));

const getUniFromNumber = number => {
  return number.toString().split("");
};

export default () => {
  const [count, setCount] = useState(10);
  const [second, setSecond] = useState([]);
  const [minute, setMinute] = useState([]);
  const [hour, setHour] = useState([]);

  function tick() {
    setCount(count - 1);

    let countRemain = count;
    const hours = Math.floor(countRemain / 3600);
    countRemain -= hours * 3600;
    const minutes = Math.floor(countRemain / 60);
    countRemain -= minutes * 60;

    setSecond(getUniFromNumber(countRemain));
    setMinute(getUniFromNumber(minutes));
    setHour(getUniFromNumber(hours));
  }

  useEffect(() => {
    const timerID = setInterval(() => tick(), 1000);
    if (count < 0) {
      clearInterval(timerID);
    }
    return function cleanup() {
      clearInterval(timerID);
    };
  });

  const classes = useStyles();

  const transitionsSecond = useTransition(second, null, {
    from: { opacity: 0, transform: "translate3d(0,-20px,0)" },
    enter: { opacity: 1, transform: "translate3d(0,0px,0)" },
    leave: { transform: "translate3d(0,0px,0)" }
  });
  const transitionsMinute = useTransition(minute, null, {
    from: { transform: "translate3d(0,-20px,0)" },
    enter: { transform: "translate3d(0,0px,0)" },
    leave: { transform: "translate3d(0,0px,0)" }
  });
  const transitionsHour = useTransition(hour, null, {
    from: { transform: "translate3d(0,-20px,0)" },
    enter: { transform: "translate3d(0,0px,0)" },
    leave: { transform: "translate3d(0,0px,0)" }
  });

  return (
    <div style={{ display: "flex" }}>
      {transitionsHour.map(({ item, props, key }) => (
        <animated.div key={key} style={props}>
          <Typography variant="h6" className={classes.timer}>
            {item}
          </Typography>
        </animated.div>
      ))}
      <Typography variant="h6" className={classes.timer}>
        :
      </Typography>
      {transitionsMinute.map(({ item, props, key }) => (
        <animated.div key={key} style={props}>
          <Typography variant="h6" className={classes.timer}>
            {item}
          </Typography>
        </animated.div>
      ))}
      <Typography variant="h6" className={classes.timer}>
        :
      </Typography>
      {transitionsSecond.map(({ item, props, key }) => (
        <animated.div key={key} style={props}>
          <Typography variant="h6" className={classes.timer}>
            {item}
          </Typography>
        </animated.div>
      ))}
    </div>
  );
};
