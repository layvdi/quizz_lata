import React from "react";
import PropTypes from "prop-types";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { makeStyles } from "@material-ui/core/styles";
import Badge from "@material-ui/core/Badge";
import clsx from "clsx";
import Typography from "@material-ui/core/Typography";

const drawerWidth = 150;

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerClose: {
    width: 0
  },
  menuButton: {
    position: "absolute",
    right: 0,
    alignSelf: "center"
  },
  toolbar: {
    ...theme.mixins.toolbar,
    display: "flex"
  },
  drawerPaper: {
    width: drawerWidth
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3)
  },
  timer: {
    flexGrow: 1,
    justifyContent: "center",
    display: "flex",
    alignItems: "center"
  },
  questionNumber: {
    margin: theme.spacing(2)
  },
  buttonQuestionNumberWrapper: {
    justifyContent: "center"
  }
}));

export default function ResponsiveDrawer(props) {
  const classes = useStyles();
  const drawer = (
    <div>
      <div className={classes.toolbar}>
        <Typography variant="h6" className={classes.timer}>
          List Question
        </Typography>
      </div>
      <Divider />
      <List>
        {["Inbox", "Starred", "Send email", "Drafts"].map((text, index) => (
          <ListItem
            className={classes.buttonQuestionNumberWrapper}
            button
            key={text}
          >
            Question:
            <Badge
              className={classes.questionNumber}
              badgeContent={index + 1}
              color="secondary"
            />
          </ListItem>
        ))}
      </List>
    </div>
  );

  return (
    <Drawer
      variant="persistent"
      anchor="left"
      open={props.isOpenDraw}
      className={clsx(classes.drawer, !props.isOpenDraw && classes.drawerClose)}
      classes={{
        paper: classes.drawerPaper
      }}
      onClose={() => props.toggleDraw(!props.isOpenDraw)}
      ModalProps={{
        keepMounted: true
      }}
    >
      {drawer}
    </Drawer>
  );
}

ResponsiveDrawer.propTypes = {
  toggleDraw: PropTypes.func.isRequired,
  isOpenDraw: PropTypes.bool.isRequired
};
