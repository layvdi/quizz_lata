import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import CloseIcon from "@material-ui/icons/Close";
import PropTypes from "prop-types";

import CountDownComponent from "../countdown/CountDownComponent";

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1
  },
  toolbarWrapper: {
    justifyContent: "space-between",
    display: "flex"
  }
}));

export default function ButtonAppBar(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="relative">
        <Toolbar className={classes.toolbarWrapper}>
          <IconButton
            onClick={() => props.toggleDraw(!props.isOpenDraw)}
            color="inherit"
            aria-label="Menu"
          >
            {props.isOpenDraw ? <CloseIcon /> : <MenuIcon />}
          </IconButton>
          <CountDownComponent />
          <p>login</p>
        </Toolbar>
      </AppBar>
    </div>
  );
}

ButtonAppBar.propTypes = {
  toggleDraw: PropTypes.func.isRequired,
  isOpenDraw: PropTypes.bool.isRequired
};
