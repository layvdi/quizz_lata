import React, { Component } from 'react'

import QuizComponent from '../components/client/quiz/QuizComponent'

class QuizPage extends Component {
  render() {
    return (
      <QuizComponent />
    )
  }
}

export default QuizPage;