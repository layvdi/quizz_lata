import App, { Container } from "next/app";
import Router from "next/router";
import NProgress from "nprogress";
import CssBaseline from "@material-ui/core/CssBaseline";
import withRedux from "next-redux-wrapper";
import { Provider } from "react-redux";
import { ThemeProvider } from "@material-ui/styles";
import JssProvider from "react-jss/lib/JssProvider";
import "../static/styles/nprogress.css";

import store from "../stores";
import RootContainer from "../containers/rootContainer";
import theme from "../ultis/materialTheme";

Router.events.on("routeChangeStart", () => {
  NProgress.start();
});
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

class CustomApp extends App {
  componentDidMount() {
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }

  render() {
    const { Component, pageProps } = this.props;
    return (
      <Container>
        <Provider store={store.store}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <RootContainer>
              <Component {...pageProps} pageContext={this.pageContext} />
            </RootContainer>
          </ThemeProvider>
        </Provider>
      </Container>
    );
  }
}

export default withRedux(() => store.store, { debug: false })(CustomApp);
