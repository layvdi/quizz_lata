import React, { Component } from "react";
import { connect } from "react-redux";

import AdminContainer from "../../../containers/admin/AdminContainer";
import ListQuestionComponent from "../../../components/admin/listQuestion/ListQuestionComponent";

class QuestionPage extends Component {
  render() {
    return (
      <AdminContainer>
        <ListQuestionComponent />
      </AdminContainer>
    );
  }
}

const mapPropsToState = state => {};

export default connect(QuestionPage);
