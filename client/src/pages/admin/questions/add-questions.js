import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import AdminContainer from "../../../containers/admin/AdminContainer";
import AddQuestionComponent from "../../../components/admin/addQuestion/AddQuestionComponent";
import * as actions from "../../../stores/actions";

const AddQuestionPage = props => {
  function requestCreateNewQuestion(data) {
    props.requestCreateNewQuestion(data);
  }
  return (
    <AdminContainer>
      <AddQuestionComponent
        requestCreateNewQuestion={requestCreateNewQuestion}
      />
    </AdminContainer>
  );
};

AddQuestionPage.propTypes = {
  requestCreateNewQuestion: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
  requestCreateNewQuestion: data =>
    dispatch(actions.requestCreateNewQuestion(data))
});

export default connect(
  undefined,
  mapDispatchToProps
)(AddQuestionPage);
