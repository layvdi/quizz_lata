import React, { Component } from "react";
import { connect } from "react-redux";
import propTypes from "prop-types";

import * as actions from "../../stores/actions";
import UsersComponents from "../../components/user/usersComponent";
import AdminContainer from "../../containers/admin/AdminContainer";

class Users extends Component {
  componentDidMount() {
    this.handleGetListUser({});
  }

  handleGetListUser = data => {
    this.props.doGetListUser(data);
  };

  render() {
    return (
      <AdminContainer>
        <UsersComponents listUser={this.props.listUser} />
      </AdminContainer>
    );
  }
}

const mapStateToProps = state => ({
  listUser: state.userReducer.listUser
});

const mapDispatchToProps = dispatch => ({
  doGetListUser: data => dispatch(actions.doGetListUser(data))
});

Users.propTypes = {
  listUser: propTypes.object.isRequired,
  doGetListUser: propTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Users);
