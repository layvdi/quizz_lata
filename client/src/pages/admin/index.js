import React, { Component } from "react";
import AdminContainer from "../../containers/admin/AdminContainer";

class Home extends Component {
  render() {
    return (
      <AdminContainer>
        <h1>Home</h1>
      </AdminContainer>
    );
  }
}

export default Home;
