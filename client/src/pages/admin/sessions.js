import React, { Component } from "react";
import AdminContainer from "../../containers/admin/AdminContainer";

class SessionPage extends Component {
  render() {
    return (
      <AdminContainer>
        <h1>Session</h1>
      </AdminContainer>
    );
  }
}

export default SessionPage;
