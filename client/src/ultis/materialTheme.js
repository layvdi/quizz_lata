import { createMuiTheme } from "@material-ui/core/styles";
import { red } from "@material-ui/core/colors";

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#1976d2"
    },
    secondary: {
      main: "#4C49A2"
    },
    success: {
      main: "#d84315"
    },
    error: {
      main: red.A400
    },
    background: {
      default: "#eee"
    }
  }
});

export default theme;
