console.log("====================================");
console.log(1111);
console.log("====================================");

import * as seeder from "mongoose-seed";
import userRepository from "../../server/src/repositories/userRepository";
import * as faker from "faker";

const dumpUsers = async () => {
  const numOfUser = 15;
  const listUser = [];
  listUser.push({
    firstName: "Admin",
    lastName: "Node Base",
    email: "admin@gmail.com",
    password: await userRepository.encryptPassword("12345678")
  });
  for (let i = 0; i < numOfUser; i++) {
    listUser.push({
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      password: await userRepository.encryptPassword("12345678")
    });
  }

  return listUser;
};

async function initUsers() {
  const listUser = await dumpUsers();
  let data = [
    {
      model: "User",
      documents: listUser
    }
  ];

  seeder.connect(process.env.MONGO_DB_URL, () => {
    seeder.loadModels(["src/models/userModel"]);
    seeder.clearModels(["User"], () => {
      seeder.populateModels(data, () => {
        seeder.disconnect();
      });
    });
  });
}

initUsers();
