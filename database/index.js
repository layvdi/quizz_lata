const fs = require("fs");
const path = require("path");
const exec = require("child_process").exec;
require("dotenv").config();

const async = require("async");

const scriptsFolder = path.join(__dirname, "./seeders");
const files = fs.readdirSync(scriptsFolder);

const funcs = files.map(function(file) {
  console.log("====================================");
  console.log(`${scriptsFolder}/${file}`);
  console.log("====================================");
  return exec.bind(null, `ts-node ${scriptsFolder}/${file}`); // execute node command
});

function getResults(err, data) {
  if (err) {
    return console.log(err);
  }
  const results = data.map(function(lines) {
    return lines.join("");
  });
  console.log(results);
}

async.series(funcs, getResults);
