export default interface IDataSign {
  _id: string;
  firstName: string;
  lastName: string;
  email: string;
  role: string;
}
