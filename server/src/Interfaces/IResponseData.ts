export interface ISuccessResponseData {
  statusCode?: number;
  data: object;
}

export type messageResponse = {
  msg: string;
  [key: string]: any;
}

export type messageValidateResponse = {
  [key: string]: messageResponse;
}
    
export interface IFailResponseData {
  statusCode?: number;
  data?: messageResponse[] | messageValidateResponse;
}