import { Request } from "express";
import IDataSign from "./IDataSign";

interface IRequest extends Request {
  user: IDataSign;
}

export default IRequest;
