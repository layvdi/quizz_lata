import { Router } from 'express';

import userController from '../../../controllers/userController';
import Authentication from "../../../middlewares/authenticationMiddleware";

const router = Router();

router.post('/login', userController.login);

router.get('/', Authentication.authorizeRequest, userController.getListUser);
router.get('/:id', Authentication.authorizeRequest, userController.getUser);
router.post('/', userController.createUser);

export default router;