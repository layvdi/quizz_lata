import { Router } from "express";

import questionController from "../../../controllers/questionController";
import Authentication from "../../../middlewares/authenticationMiddleware";

const router = Router();

router.get(
  "/",
  Authentication.authorizeRequest,
  questionController.getQuestions
);
router.post("/", Authentication.authorizeRequest, questionController.create);

export default router;
