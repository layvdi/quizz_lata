import { Router } from "express";

import user from "./v1/userApiRoute";
import question from "./v1/questionApiRoute";

const router = Router();

router.use("/v1/users", user);
router.use("/v1/questions", question);

export default router;
