import Repository from "../repositories/repository";

class Service {
  private repository: Repository<any>;
  public constructor(repository: Repository<any>) {
    this.repository = repository;
  }

  public async findById(id: string): Promise<any> {
    return await this.repository.findById(id);
  }

  public async getList(query?: object): Promise<any> {
    return await this.repository.findWithPagination(query);
  }

  public async create(item: any): Promise<any> {
    return await this.repository.create(item);
  }
}

export default Service;
