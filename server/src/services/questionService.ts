import Service from "./service";
import questionRepository from "../repositories/questionRepository";

class QuestionService extends Service {
  public constructor() {
    super(questionRepository);
  }
}

export default new QuestionService();
