import Service from "./service";
import userRepository from "../repositories/userRepository";
import JWTUtil from "../utils/jwtUtil";
import IDataSign from "../Interfaces/IDataSign";

class UserService extends Service {
  public constructor() {
    super(userRepository);
  }

  public async checkLogin(email: string, password: string): Promise<any> {
    const userCompare = await userRepository.findOne({ email });

    if (!userCompare) {
      return null;
    }

    if (!userRepository.comparePassword(password, userCompare.password)) {
      return null;
    }

    const dataSign: IDataSign = {
      _id: userCompare._id,
      email: userCompare.email,
      firstName: userCompare.firstName,
      lastName: userCompare.lastName,
      role: "user"
    };

    const accessToken = JWTUtil.sign(dataSign);

    return { user: dataSign, accessToken };
  }
}

export default new UserService();
