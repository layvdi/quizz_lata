require('express-async-errors');
require('express-async-errors');
import * as dotenv from "dotenv";
dotenv.config();

import app from "./app";

const PORT = process.env.PORT || 3000;

app.listen(PORT, (): void => {
  // tslint:disable-next-line
  console.log(`Server is running on port: ${PORT}`);
});
