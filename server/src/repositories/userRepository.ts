import Repository from "./repository";
import userModel, { IUser } from "../models/userModel";
import * as bcrypt from "bcrypt";

class UserRepository extends Repository<IUser> {
  public constructor() {
    super(userModel);
  }

  public comparePassword(password: string, passwordCompare: string): boolean {
    return bcrypt.compareSync(password, passwordCompare);
  }

  public encryptPassword(password: string): Promise<string> {
    const hashed: Promise<string> = bcrypt.hash(password, 10);
    return hashed;
  }
}

export default new UserRepository();
