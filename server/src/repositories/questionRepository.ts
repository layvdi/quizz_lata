import Repository from "./repository";
import questionModel, { IQuestion } from "../models/questionModel";
import * as bcrypt from "bcrypt";

class QuestionRepository extends Repository<IQuestion> {
  public constructor() {
    super(questionModel);
  }
}

export default new QuestionRepository();
