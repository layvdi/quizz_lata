import * as mongoose from "mongoose";

interface IModel<T extends mongoose.Document>
  extends mongoose.PaginateModel<T> {}

abstract class Repository<T extends mongoose.Document> {
  public readonly _collection: IModel<T>;

  public constructor(collection: IModel<T>) {
    this._collection = collection;
  }

  public create(item: T): Promise<T> {
    const result: Promise<T> = this._collection.create(item);
    return result;
  }

  public update(item: T, id: string): mongoose.DocumentQuery<T | null, T> {
    let result: mongoose.DocumentQuery<T | null, T>;

    result = this._collection.findOneAndUpdate({ _id: id }, item, {
      new: true
    });

    return result;
  }

  public delete(query: any): Promise<boolean> {
    const result = this._collection
      .find(query)
      .remove()
      .exec();
    return result;
  }

  public find(query: any): mongoose.DocumentQuery<T[], T> {
    const result: mongoose.DocumentQuery<T[], T> = this._collection.find(query);
    return result;
  }

  public findWithPagination(
    query: any,
    option: any = {}
  ): Promise<mongoose.PaginateResult<T>> {
    const result: Promise<
      mongoose.PaginateResult<T>
    > = this._collection.paginate(query, option);
    return result;
  }

  public findOne(query: any): mongoose.DocumentQuery<T | null, T> {
    return this._collection.findOne(query);
  }

  public findById(id: string): mongoose.DocumentQuery<T | null, T> {
    return this._collection.findById(id);
  }
}

export default Repository;
