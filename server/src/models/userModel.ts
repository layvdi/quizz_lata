import * as mongoose from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";

export interface IUser extends mongoose.Document {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
}

const UserSchema: mongoose.Schema = new mongoose.Schema(
  {
    email: { type: String, required: true, unique: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    password: { type: String, required: true }
  },
  { timestamps: true }
);

UserSchema.plugin(mongoosePaginate);

interface IUserModel<T extends mongoose.Document>
  extends mongoose.PaginateModel<T> {}

const UserModel: IUserModel<IUser> = mongoose.model<IUser>("User", UserSchema);

export default UserModel;
