import * as mongoose from "mongoose";
import * as mongoosePaginate from "mongoose-paginate";

export enum QuestionType {
  "oneChoice" = "oneChoice",
  "multiChoice" = "multiChoice"
}

export enum QuestionStatus {
  "pending" = "pending",
  "active" = "active",
  "rejected" = "rejected"
}

export type Answer = {
  content: String;
  isRight: boolean;
};

export interface IQuestion extends mongoose.Document {
  content: string;
  answer: Array<Answer>;
  type: QuestionType;
  status: QuestionStatus;
  createdBy: string;
  editedBy: string;
}

const QuestionSchema: mongoose.Schema = new mongoose.Schema(
  {
    content: {
      type: mongoose.Schema.Types.String,
      required: true,
      unique: true
    },
    answer: { type: mongoose.Schema.Types.Array, required: true },
    type: { type: mongoose.Schema.Types.String, required: true },
    status: { type: mongoose.Schema.Types.String, required: true },
    createdBy: { type: mongoose.Schema.Types.ObjectId, required: true },
    editedBy: { type: mongoose.Schema.Types.ObjectId, required: true }
  },
  { timestamps: true }
);

QuestionSchema.plugin(mongoosePaginate);

interface IQuestionModel<T extends mongoose.Document>
  extends mongoose.PaginateModel<T> {}

const QuestionModel: IQuestionModel<IQuestion> = mongoose.model<IQuestion>(
  "Question",
  QuestionSchema
);

export default QuestionModel;
