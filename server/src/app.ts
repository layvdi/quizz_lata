import * as bodyParser from "body-parser";
import * as express from "express";
import * as next from "next";
import * as expressValidator from "express-validator";

import connectDB from "./utils/databaseUtil";
import apiRouter from "./routers/api/apiRoute";
import { middlewareHandleException } from "./middlewares/errorHandlerMiddleware";
import logger from "./utils/loggerUtil";
import * as morgan from "morgan";

const clientSource = process.env.CLIENT_SOURCE_PATH;

class App {
  public app: express.Application;

  public constructor() {
    this.initApp();
  }

  private async initApp(): Promise<void> {
    connectDB(process.env.MONGO_DB_URL);
    this.app = express();
    this.configApp();
    this.initRoute();
    this.initMiddleware();

    if (clientSource) {
      await this.initNextServer();
      return;
    }
  }

  private initMiddleware(): void {
    this.app.use(middlewareHandleException);
  }

  public async initNextServer(): Promise<void> {
    const nextApp: next.Server = next({ dev: true, dir: clientSource });
    const handleRouter = nextApp.getRequestHandler();

    await nextApp.prepare();

    this.app.get(
      "*",
      (req, res): Promise<void> => {
        return handleRouter(req, res);
      }
    );

    return;
  }

  private configApp(): void {
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(express.json());
    this.app.use(expressValidator());
    this.app.use(
      morgan("combined", {
        stream: {
          write: (message: any) => {
            if (message.includes("_next")) {
              return;
            }
            logger.info(message);
          }
        }
      })
    );
  }

  private initRoute(): void {
    this.app.use("/api", apiRouter);
  }
}

export default new App().app;
