import { NextFunction, Request, Response } from 'express';
import logger from '../utils/loggerUtil';
import { buildFailResponse } from '../utils/responseUtil'

class HttpException extends Error {
  public statusCode: number;
  public messageResponse: string;
  
  public constructor(statusCode: number, message: string) {
    super(message);
    this.statusCode = statusCode;
    this.message = message;
  }
}

// eslint-disable-next-line
export const middlewareHandleException = (error: HttpException, request: Request, response: Response, next: NextFunction): any => {
  logger.error(error)
  
  let { statusCode, messageResponse } = error;

  if (!statusCode) {
    statusCode = 500;
  }

  if (!messageResponse) {
    messageResponse = 'Internal server error';
  }

  return buildFailResponse(response, { statusCode, data: [{msg: messageResponse}] });
};