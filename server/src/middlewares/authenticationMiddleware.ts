import { NextFunction, Response } from "express";
import * as httpSatusCode from 'http-status-codes';

import JWTUtil from "../utils/jwtUtil";
import IDataSign from "../Interfaces/IDataSign";
import IRequest from "../Interfaces/IRequest";
import * as responseUtil from '../utils/responseUtil'

class Authentication {
  public authorizeRequest(req: IRequest, res: Response, next: NextFunction): object {
    let token: string = (req.headers["x-access-token"] || req.headers.authorization || "") as string;

    if (token.startsWith("Bearer ")) {
      token = token.slice(7, token.length);
    }

    if (token) {
      try {
        const resultVerify = JWTUtil.verify(token);
        if (resultVerify) {
          req.user = resultVerify as IDataSign;
          next();
          return;
        }
      } catch (ex) {
        if (ex.name === "TokenExpiredError") {
          return responseUtil.buildFailResponse(res, { statusCode: httpSatusCode.UNAUTHORIZED, data: [{ msg: "Token expired!" }]});
        }

        return responseUtil.buildFailResponse(res, { statusCode: httpSatusCode.UNAUTHORIZED, data: [{ msg: "Unauthorized user!" }]});
      }
    }

    return responseUtil.buildFailResponse(res, { statusCode: httpSatusCode.UNAUTHORIZED, data: [{ msg: "Unauthorized user!" }]});
  }
}

export default new Authentication();
