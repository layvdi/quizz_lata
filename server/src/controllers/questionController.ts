import { Response } from "express";
import * as statusCode from "http-status-codes";

import { toArray } from "lodash";
import IRequest from "../Interfaces/IRequest";
import Controller from "./controller";
import questionService from "../services/questionService";
import { messageValidateResponse } from "../Interfaces/IResponseData";
import { QuestionStatus, QuestionType } from "../models/questionModel";

class QuestionController extends Controller {
  public constructor() {
    super();
  }

  public async getQuestions(req: IRequest, res: Response): Promise<object> {
    const question = await questionService.getList();

    return this.successResponse(res, { data: question });
  }

  public async create(req: IRequest, res: Response): Promise<object> {
    req.checkBody("content").notEmpty();
    req
      .checkBody("answer")
      .isArray()
      .notEmpty();
    req
      .checkBody("type")
      .isIn([QuestionType.multiChoice, QuestionType.oneChoice]);
    req
      .checkBody("tag")
      .isArray()
      .notEmpty();

    console.log("====================================");
    console.log(req.user);
    console.log("====================================");

    const errors = req.validationErrors() as messageValidateResponse;
    if (errors) {
      return this.failResponse(res, { data: errors });
    }

    const newQuestion = {
      content: req.body.content,
      answer: req.body.answer,
      type: req.body.type,
      status: QuestionStatus.pending,
      createdBy: "507f1f77bcf86cd799439011",
      editedBy: "507f1f77bcf86cd799439011"
    };

    const question = await questionService.create(newQuestion);

    return this.successResponse(res, { data: question });
  }
}

export default new QuestionController();
