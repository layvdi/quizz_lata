import { Response } from "express";
const autoBind = require("auto-bind");
import * as _ from "lodash";

import { ISuccessResponseData, IFailResponseData } from '../Interfaces/IResponseData';
import * as responseUtil from "../utils/responseUtil";

export class Controller {
  public constructor() {
    autoBind(this);
  }

  public successResponse(res: Response, dataResponse: ISuccessResponseData): object {
    let newDataResponse: ISuccessResponseData = dataResponse;

    if (_.isObject(dataResponse.data) && dataResponse.data["docs"]) {
      newDataResponse = this.buildSuccessResponseWithPagination(dataResponse);
    }

    return responseUtil.buildSuccessResponse(res, newDataResponse);
  }

  public failResponse(res: Response, dataResponse: IFailResponseData): object {
    return responseUtil.buildFailResponse(res, dataResponse);
  }

  private buildSuccessResponseWithPagination(dataResponse: ISuccessResponseData): ISuccessResponseData {
    const listData = dataResponse.data;
    const items: any = _.get(listData, "docs");

    return {
      statusCode: dataResponse.statusCode,
      data: {
        items,
        pagination: _.omit(listData, "docs")
      }
    };
  }
}

export default Controller;
