import { Response } from "express";
import * as statusCode from "http-status-codes";

import IRequest from "../Interfaces/IRequest";
import Controller from "./controller";
import { IUser } from "../models/userModel";
import { messageValidateResponse } from '../Interfaces/IResponseData';
import userService from '../services/userService';

class UserController extends Controller {
  public constructor() {
    super();
  }

  public async getUser(req: IRequest, res: Response): Promise<object> {
    req.checkParams("id").notEmpty();

    const errors = req.validationErrors() as messageValidateResponse;

    if (errors) {
      return this.failResponse(res, {data: errors});
    }

    const user = await userService.findById(req.params.id);

    return this.successResponse(res,  {data: user});
  }

  public async createUser(req: IRequest, res: Response): Promise<object> {
    const newUser = {
      email: "lay.vdi@gmail.com",
      firstName: "lay",
      lastName: "vo",
      password: "1234"
    } as IUser;

    const result = await userService.create(newUser);

    return this.successResponse(res,  {data: result});
  }

  public async login(req: IRequest, res: Response): Promise<object> {
    req.checkBody("email").isEmail();
    req.checkBody("password").notEmpty();

    const errors = req.validationErrors() as messageValidateResponse;
    if (errors) {
      return this.failResponse(res, {data: errors});
    }

    const result = await userService.checkLogin(req.body.email, req.body.password);
    
    if(!result) {
      return this.failResponse(res, { statusCode: statusCode.UNAUTHORIZED, data: [{msg: "Invalid user name or password"}]});
    }

    return this.successResponse(res, result);
  }

  public async getListUser(req: IRequest, res: Response): Promise<object> {
    const listUser = await userService.getList({});

    return this.successResponse(res, {data: listUser});
  }
}

export default new UserController();
