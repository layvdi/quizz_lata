import * as winston from 'winston';
import * as winstonDailyRotateFile from 'winston-daily-rotate-file';
import * as path from 'path';
import * as logform from 'logform';
import * as Transport from 'winston-transport';

const pathLog = 'log/';
class Logger {
  public logger: winston.Logger;
  
  public constructor() {
    const loggerConfig: winston.LoggerOptions = this.initConfig();

    this.logger = winston.createLogger(loggerConfig);

    if (process.env.NODE_ENV !== 'production') {
      this.logger.add(new winston.transports.Console());
    }
  }

  private initConfig(): winston.LoggerOptions{
    const exceptionHandlers = [ this.configException() ];
    const transports = [ this.configTransport() ];
    const format = this.configFormat();
    return {
      levels: winston.config.syslog.levels,
      exceptionHandlers,
      transports,
      format,
      exitOnError: true,
    }
  }

  private configException(): winston.transports.FileTransportInstance {
    return new winston.transports.File({
      filename: path.join(pathLog, 'error.log'),
    });
  }

  private configFormat(): logform.Format {
    return winston.format.combine(
      winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
      winston.format.splat(),
      winston.format.printf((info): string => {
        const {
          timestamp, level, message, stack
        } = info;

        return `[${level}]: ${timestamp} - ${message} ${stack || ''}`;
      }),
    );
  }

  private configTransport (): Transport {
    return new winstonDailyRotateFile({
      filename: path.join(pathLog, 'application-%DATE%.log'),
      datePattern: 'YYYY-MM-DD',
      zippedArchive: false,
      maxSize: '10m',
      maxFiles: '14d',
      level: process.env.LOG_LEVEL || 'info',
    })
  }
}

const logger  = new Logger().logger;

export default logger;