import * as fs from "fs";
import * as jwt from "jsonwebtoken";
import IDataSign from "../Interfaces/IDataSign";
import * as path from 'path';

const privateKey = fs.readFileSync(path.resolve(__dirname, '../../configs/jwtKey/private.key'));
const publicKey = fs.readFileSync(path.resolve(__dirname, '../../configs/jwtKey/public.key'))

class JWTUtil {
  public signOptions: object;
  public constructor() {
    this.signOptions = {
      expiresIn:
        process.env.JWT_OPTION_ALGORITHM_EXPIRES_IN || 60 * 60 * 24 * 30,
      algorithm: "RS256"
    };
  }

  public sign(data: IDataSign): string {
    return jwt.sign(data, privateKey, this.signOptions);
  }

  public verify(token: string): any {
    try {
      const verified = jwt.verify(token, publicKey, this.signOptions);
      return verified;
    } catch (ex) {
      throw ex;
    }
  }
}

export default new JWTUtil(); 
