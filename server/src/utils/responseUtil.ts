import { Response } from "express";
import * as httpStatusCode from "http-status-codes";
import { ISuccessResponseData, IFailResponseData } from '../Interfaces/IResponseData';

export const buildSuccessResponse = (res: Response, dataResponse: ISuccessResponseData): object => {
  if(!dataResponse.statusCode) {
    dataResponse.statusCode = httpStatusCode.OK
  }

  return res.status(dataResponse.statusCode).send(dataResponse);
};

export const buildFailResponse = (res: Response, dataResponse: IFailResponseData): object => {
  if(!dataResponse.statusCode) {
    dataResponse.statusCode = httpStatusCode.BAD_REQUEST
  }
  
  return res.status(dataResponse.statusCode).send(dataResponse);
};
