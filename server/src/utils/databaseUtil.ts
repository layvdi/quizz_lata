import * as mongoose from "mongoose";

const connectDB = (dbURL): void => {
  mongoose.connect(dbURL, { useNewUrlParser: true });
  mongoose.set("useCreateIndex", true);

  mongoose.connection.on(
    "connected",
    (): void => {
      console.log("Mongoose default connection is open to ", dbURL); // eslint-disable-line
    }
  );

  mongoose.connection.on(
    "error",
    (err): void => {
      console.log("Mongoose default connection has occured " + err + " error"); // eslint-disable-line
    }
  );

  mongoose.connection.on(
    "disconnected",
    (): void => {
      console.log("Mongoose default connection is disconnected"); // eslint-disable-line
    }
  );

  process.on(
    "SIGINT",
    (): void => {
      mongoose.connection.close(
        (): void => {
          console.log("Mongoose default connection is disconnected due to application termination"); // eslint-disable-line
          process.exit(0);
        }
      );
    }
  );
};

export default connectDB;
